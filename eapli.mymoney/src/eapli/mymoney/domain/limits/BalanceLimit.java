package eapli.mymoney.domain.limits;

import eapli.framework.model.Money;
import eapli.framework.patterns.DomainEvent;
import eapli.mymoney.domain.Book;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.Persistence;
import javax.persistence.Entity;

/**
 * Defines limit values over balance.
 *
 * @author nuno
 */
@Entity
public class BalanceLimit extends AbstractLimit {

	private BalanceLimit() {
		super();
	}

	/**
	 * Balance AbstractLimit constructor.
	 *
	 * @param yellowLimit value for yellow limit, should be higher than red
	 * limit
	 * @param redLimit value for red limit, should be lower than yellow limit
	 */
	public BalanceLimit(final Money yellowLimit, final Money redLimit) {
		super(yellowLimit, redLimit);
	}

	/**
	 * Blackbox to get current balance
	 *
	 * @return Current book balance
	 */
	private Money getCurrentBalance() {

		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = repo.theBook();
		return theBook.currentMonth().balance();
	}

	/**
	 * Retuns
	 *
	 * @param expense
	 * @return
	 */
	@Override
	public boolean isLimitExceeded(DomainEvent eventRegistered) {
		if (eventRegistered == null) {
			throw new IllegalArgumentException("DomainEventBase is null");
		}

		Money currentBalance = getCurrentBalance();

		return isRedLimitExceeded(currentBalance)
			|| isYellowLimitExceeded(currentBalance);

	}
}
