/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToOne;

/**
 *
 * @author mcn
 */
@Entity
@Inheritance
public class Payment {

	// TODO should this class be public or should the Expense be the only one to have access to Payments?
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne(cascade = CascadeType.REFRESH)
	private PaymentMean mean;

	public Payment(PaymentMean mean) {
		if (mean == null) {
			throw new IllegalArgumentException();
		}
		this.mean = mean;
	}

	/**
	 * for ORM tool only
	 */
	public Payment() {
	}

	public String description() {
		return mean.description();
	}
}
