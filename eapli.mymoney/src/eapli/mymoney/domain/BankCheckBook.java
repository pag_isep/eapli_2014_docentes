/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.util.Validations;
import javax.persistence.Entity;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class BankCheckBook extends PaymentMean {

	private String bankName;
	// TODO account number is not actually a "number" but a string of digits
	private long accountNumber;

	public BankCheckBook() {
	}

	public BankCheckBook(final String bankName, final long accountNumber) {
		super();
		if (bankName == null || bankName.trim().isEmpty()) {
			throw new IllegalArgumentException();
		}

		if (!Validations.isPositiveNumber(accountNumber)) {
			throw new IllegalArgumentException();
		}

		this.bankName = bankName;
		this.accountNumber = accountNumber;
	}

	@Override
	public String description() {
		return "BANK CHECK BOOK -Bank:" + bankName + " Account Number:" + accountNumber;
	}

}
