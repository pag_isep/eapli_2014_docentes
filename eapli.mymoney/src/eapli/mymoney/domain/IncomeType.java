/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.DomainEntity;
import eapli.util.Validations;

/**
 *
 * @author Nuno
 */
public class IncomeType implements DomainEntity<String> {
	//TODO há bastante duplicação entre IncomeType e ExpenseType
	// deviamos fazer um refactoring

	private final String text;

	public IncomeType(final String text) {
		if (Validations.isNullOrWhiteSpace(text)) {
			throw new IllegalArgumentException();
		}
		this.text = text;
	}

	/**
	 * returns the description text of this income type
	 *
	 * this method is only provided for user output
	 *
	 * @return
	 */
	public String description() {
		return text;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof IncomeType)) {
			return false;
		}
		IncomeType itOther = (IncomeType) other;
		//TODO make case validation and corresponding unit test also for ExpenseType [NFE 17-04-2014]
		return this.text.toLowerCase().equals(itOther.text.toLowerCase());
	}

	@Override
	//TODO check purpose and algorithm/constants
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + (this.text != null ? this.text.hashCode() : 0);
		return hash;
	}

	@Override
	public String id() {
		return text;
	}
}
