/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import java.util.Calendar;
import javax.persistence.Entity;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class DebitCard extends PaymentCard {

	public DebitCard() {
	}

	public DebitCard(final String bankName,
					 final Calendar validity,
					 final long cardNumber) {
		super(bankName, validity, cardNumber);
	}

	@Override
	public String description() {
		return "DEBIT CARD -" + super.description();
	}
}
