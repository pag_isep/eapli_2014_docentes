/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

//import eapli.expensemanager.domain.Money;
import eapli.framework.model.Money;
import eapli.framework.patterns.DomainEntity;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author arocha
 */
// TODO até ao momento não há nenhum requisto que obrigue a que esta classe seja publica
// pode ser uma classe visivel apenas para o Book
// TODO check if this should be a Value Object or an Entity
@Entity
public class InitialBalance implements DomainEntity<Long>, Serializable {

	@Id
	@GeneratedValue
	private Long id;

	@OneToOne(cascade = CascadeType.ALL)
	private Money howMuch;

	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar startDate;

	public InitialBalance() {
	}

	public InitialBalance(final Calendar startDate, final Money howMuch) {
		if (howMuch == null || startDate == null
			|| howMuch.lessThanOrEqual(Money.euros(0))) {
			throw new IllegalArgumentException();
		}
		this.howMuch = howMuch;
		this.startDate = startDate;
	}

	public Money getInitialBalance() {
		return howMuch;
	}

	// TODO if this class is a Value Object this methd must be removed
	@Override
	public Long id() {
		return id;
	}

	public Calendar getStartDate() {
		return startDate;
	}
}
