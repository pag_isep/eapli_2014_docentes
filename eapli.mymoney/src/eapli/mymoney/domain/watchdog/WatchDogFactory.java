/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.watchdog;

import java.util.Observer;

/**
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt>
 */
public class WatchDogFactory {

	/**
	 * Creates a WatchDog for Expenditure Limits
	 *
	 * @param observer Object that wished to receive WatchDog notifications
	 * @return Object that handles Expenditure Limits
	 */
	public static WatchDog buildLimitWatchDog(Observer observer) {
		WatchDog limitWatchDog = new LimitWatchDog();
		limitWatchDog.addObserver(observer);
		return limitWatchDog;
	}
}
