/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.watchdog;

import eapli.framework.patterns.DomainEvent;
import eapli.mymoney.domain.events.alert.AlertLimitEventFactory;
import eapli.mymoney.domain.limits.AbstractLimit;
import eapli.mymoney.persistence.LimitRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Observable;

/**
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt>
 */
public class LimitWatchDog extends WatchDog {

	private void publish(DomainEvent eventBase) {
		setChanged();
		notifyObservers(eventBase);
	}

	@Override
	public void update(Observable o, Object arg) {
		boolean limitExceeded;
		LimitRepository limitRepository;
		DomainEvent expenseRegistered;
		DomainEvent newEventBase;

		//TODO: NMB how to remove the following cast? (create innerclasses that only handle this kind of event)
		expenseRegistered = (DomainEvent) arg;

		limitRepository = Persistence.getRepositoryFactory().
			getLimitRepository();

		// TODO considerar ter uma cache para não questionar constantemente o repositório
		// TODO uma vez que apenas temos um limite para saldo ( ver método setBalanceLimit() ) porque não ir diretamente ao repositorio buscar esse objeto - getBalanceLimit() - em vez de obter todos os limites?
		// com os requisitos existentes não há multiplos limites
		for (AbstractLimit limit : limitRepository.all()) {
			limitExceeded = limit.isLimitExceeded(expenseRegistered);
			if (limitExceeded) {
				newEventBase = AlertLimitEventFactory.
					buildAlertEvent(limit, expenseRegistered);
				publish(newEventBase);
			}
		}
	}
}
