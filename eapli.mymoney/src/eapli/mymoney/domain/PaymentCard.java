/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.util.Validations;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PaymentCard extends PaymentMean {

	protected String bankName;
	@Temporal(javax.persistence.TemporalType.DATE)
	protected Calendar validity;
	// TODO card number is not actually a "number" but a string of digits
	protected long cardNumber;

	public PaymentCard() {
	}

	public PaymentCard(final String bankName,
					   final Calendar validity,
					   final long cardNumber) {
		super();
		if (Validations.isNullOrEmpty(bankName) || Validations.
			isNullOrWhiteSpace(bankName)) {
			throw new IllegalArgumentException();
		}

		if (validity == null) {
			throw new IllegalArgumentException();
		}

		if (!Validations.isPositiveNumber(cardNumber)) {
			throw new IllegalArgumentException();
		}

		this.bankName = bankName;
		this.validity = validity;
		this.cardNumber = cardNumber;
	}

	public String description() {
		return "Bank Name:" + this.bankName
			+ " Card Number:" + this.cardNumber
			+ " Validity: " + validity.get(Calendar.DAY_OF_MONTH) + "/"
			+ validity.get(Calendar.MONTH) + "/"
			+ validity.get(Calendar.YEAR);
	}

}
