/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import javax.persistence.Entity;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class Cash extends PaymentMean {

	public Cash() {
		id = 1L;
	}

	@Override
	public String description() {
		return "Cash";
	}

}
