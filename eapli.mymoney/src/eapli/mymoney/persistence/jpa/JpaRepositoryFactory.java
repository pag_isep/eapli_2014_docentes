/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.LimitRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class JpaRepositoryFactory implements RepositoryFactory {

	@Override
	public BookRepository getBookRepository() {
		return new eapli.mymoney.persistence.jpa.BookRepositoryImpl();
	}

	@Override
	public MonthPageRepository getMonthPageRepository() {
		return new eapli.mymoney.persistence.jpa.MonthPageRepositoryImpl();
	}

	@Override
	public ExpenseTypeRepository getExpenseTypeRepository() {
		return new eapli.mymoney.persistence.jpa.ExpenseTypeRepositoryImpl();
	}

	@Override
	public IncomeTypeRepository getIncomeTypeRepository() {
		return new eapli.mymoney.persistence.jpa.IncomeTypeRepositoryImpl();
	}

	@Override
	public WalletRepository getWalletRepository() {
		return new eapli.mymoney.persistence.jpa.WalletRepositoryImpl();
	}

	@Override
	public LimitRepository getLimitRepository() {
		return new eapli.mymoney.persistence.jpa.LimitRepositoryImpl();
	}
}
