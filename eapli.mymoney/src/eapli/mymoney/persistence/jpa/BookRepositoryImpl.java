/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.Book;
import eapli.mymoney.domain.BookID;
import eapli.mymoney.persistence.BookRepository;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class BookRepositoryImpl extends JpaRepository<Book, Long> implements BookRepository {

	private static Book instance = null;

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}

	@Override
	public Book theBook() {
		if (instance == null) {
			Collection<Book> theBooks = first(2);

			// currently there can not be more than 1 book
			if (theBooks.size() > 1) {
				throw new IllegalStateException("Something went terribly bad. multiple books cannot exist, and yet, here they are.");
			}

			if (theBooks.size() == 1) {
				Iterator<Book> iterator = theBooks.iterator();
				instance = iterator.next();
			} else {
				instance = new Book(BookID.MY_BOOK);
			}

		}
		return instance;

	}
}
