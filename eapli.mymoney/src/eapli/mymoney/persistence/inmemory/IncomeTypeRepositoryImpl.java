/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Nuno
 */
public class IncomeTypeRepositoryImpl implements IncomeTypeRepository {
	//TODO há bastante duplicação entre IncomeType e ExpenseType
	// deviamos fazer um refactoring

	private static final List<IncomeType> data = new ArrayList<IncomeType>();

	@Override
	public void add(IncomeType incomeType) {
		if (incomeType == null) {
			throw new IllegalArgumentException();
		}
		if (data.contains(incomeType)) {
			//TODO rever se deviamos ter outra exceção mais significativa
			throw new IllegalStateException();
		}
		data.add(incomeType);
	}

	@Override
	public long size() {
		return data.size();
	}

	//TODO check if we really need this method
	public boolean contains(IncomeType incomeType) {
		return data.contains(incomeType);
	}

	@Override
	public List<IncomeType> all() {
		return Collections.unmodifiableList(data);
	}
}
