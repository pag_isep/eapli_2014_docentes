/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    @Override
    public BookRepository getBookRepository() {
        return new eapli.mymoney.persistence.inmemory.BookRepositoryImpl();
    }

    @Override
    public MonthPageRepository getMonthPageRepository() {
        return new eapli.mymoney.persistence.inmemory.MonthPageRepositoryImpl();
    }

    @Override
    public ExpenseTypeRepository getExpenseTypeRepository() {
        return new eapli.mymoney.persistence.inmemory.ExpenseTypeRepositoryImpl();
    }

    @Override
    public IncomeTypeRepository getIncomeTypeRepository() {
        return new eapli.mymoney.persistence.inmemory.IncomeTypeRepositoryImpl();
    }

    @Override
    public WalletRepository getWalletRepository() {
        return new eapli.mymoney.persistence.inmemory.WalletRepositoryImpl();
    }

    @Override
    public LimitRepositoryImpl getLimitRepository() {
        return new eapli.mymoney.persistence.inmemory.LimitRepositoryImpl();
    }
}
