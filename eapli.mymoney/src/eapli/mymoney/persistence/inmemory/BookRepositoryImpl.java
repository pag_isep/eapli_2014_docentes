/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.Book;
import eapli.mymoney.domain.BookID;
import eapli.mymoney.persistence.BookRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class BookRepositoryImpl implements BookRepository {

	/**
	 * singleton representing the one and only book of the application
	 */
	private final static Book instance = new Book(BookID.MY_BOOK);

	@Override
	public Book theBook() {
		return instance;
	}

	@Override
	public Book save(Book theBook) {
		//To Do
		return instance;
	}
}
