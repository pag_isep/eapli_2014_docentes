/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.limits.AbstractLimit;
import eapli.mymoney.domain.limits.BalanceLimit;
import eapli.mymoney.persistence.LimitRepository;
import java.util.ArrayList;
import java.util.List;

/**
 * In memory AbstractLimit Repository.
 *
 * @author nuno
 */
public class LimitRepositoryImpl implements LimitRepository {

	/**
	 * Represents an in-memory list of Limits.
	 */
	//private static final List<AbstractLimit> LIMIT_LIST = new ArrayList<AbstractLimit>();
	/**
	 * Keep the only possible object that defines limits over balance.
	 */
	private static AbstractLimit theBalanceLimit = null;

//	@Override
//	public void add(final AbstractLimit limit) {
//		if (limit == null) {
//			throw new IllegalArgumentException();
//		}
//		LIMIT_LIST.add(limit);
//	}
	@Override
	public void setBalanceLimit(BalanceLimit balanceLimit) {
		if (balanceLimit == null) {
			throw new IllegalArgumentException("Limit should not be null");
		} else {
			theBalanceLimit = balanceLimit;
		}
	}

	@Override
	public List<AbstractLimit> all() {
		List<AbstractLimit> all = new ArrayList<AbstractLimit>();
		all.add(theBalanceLimit);
		return all;
	}
}
