/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.limits.AbstractLimit;
import eapli.mymoney.domain.limits.BalanceLimit;
import java.util.List;

/**
 * This interface represents the access to Limits Repository.
 *
 * @author nuno
 */
public interface LimitRepository {

	/**
	 * Set the new Balance Limit.
	 *
	 * @param limit the new balance limit
	 */
	void setBalanceLimit(BalanceLimit limit);

	/**
	 * Returns the list of all existing limits
	 *
	 * @return List of all existing limits
	 */
	public List<AbstractLimit> all();
}
