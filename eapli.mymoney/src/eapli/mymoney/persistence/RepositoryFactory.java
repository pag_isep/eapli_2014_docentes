/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface RepositoryFactory {

    BookRepository getBookRepository();

    MonthPageRepository getMonthPageRepository();

    ExpenseTypeRepository getExpenseTypeRepository();
    
    IncomeTypeRepository getIncomeTypeRepository();

    WalletRepository getWalletRepository();

    /**
     * Returns the interface of any kind of Limit Repository implementation.
     *
     * @return any kind of Limit Repository
     */
    LimitRepository getLimitRepository();
}
