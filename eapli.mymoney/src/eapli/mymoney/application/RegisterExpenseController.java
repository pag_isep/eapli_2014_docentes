/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.model.Money;
import eapli.mymoney.domain.Book;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.domain.PaymentMean;
import eapli.mymoney.domain.balancerecalculator.Recalculator;
import eapli.mymoney.domain.watchdog.WatchDog;
import eapli.mymoney.domain.watchdog.WatchDogFactory;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class RegisterExpenseController extends BaseController {

	private Observer alertObserver = null;

	public void registerExpense(final String what,
								final Calendar when,
								final Money howMuch,
								final ExpenseType type,
								final PaymentMean paymentMean,
								final String chequeNumber) {
		final BookRepository bookRepo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = bookRepo.theBook();

		delegateObserverTo(theBook);

		Recalculator theRecalculator = new Recalculator();
		theBook.addObserver(theRecalculator);

		MonthPage month = theBook.
			registerExpense(what, howMuch, when, type, paymentMean, chequeNumber);

		final MonthPageRepository pagesRepo = Persistence.getRepositoryFactory().
			getMonthPageRepository();

		pagesRepo.save(month);
	}

	public List<PaymentMean> getPaymentMeans() {
		return Persistence.getRepositoryFactory().getWalletRepository().
			theWallet().paymentMeans();
	}

	/**
	 * Add a UI as an object that wants to be an observer.
	 *
	 * @param ui UI observer
	 */
	public void addObserver(Observer alertObserver) {
		this.alertObserver = alertObserver;
	}

	/**
	 * Checks if there is an UI that want to be an observer.
	 *
	 * @return True if the UI wishes to be an observer, False otherwise.
	 */
	private boolean hasUIObserver() {
		return alertObserver != null;
	}

	/**
	 * Delegates the observer if the UI wishes to receive notifications
	 */
	private void delegateObserverTo(Observable observable) {
		if (hasUIObserver()) {
			WatchDog watchDog = WatchDogFactory.
				buildLimitWatchDog(alertObserver);
			observable.addObserver(watchDog);
		}
	}
}
