/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.model.Money;
import eapli.mymoney.domain.Book;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;

/**
 *
 * @author arocha
 */
public class RegisterInitialBalanceController extends BaseController {

	public void registerInitialBalance(final Calendar startDate,
									   final Money howMuch) {
		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = repo.theBook();
		theBook.registerInitialBalance(startDate, howMuch);

		repo.save(theBook);
	}

	public boolean existsInitialBalance() {
		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = repo.theBook();
		return theBook.hasInitialBalance();

	}

}
