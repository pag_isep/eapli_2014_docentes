/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.model.Money;
import eapli.mymoney.domain.Book;
import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.Persistence;
//import eapli.framework.model.Money;

/**
 * All Controller must extend this class
 *
 * @author arocha
 */
public abstract class BaseController {

	//TODO should we refactor this class and move it to the framework project?
	public Money getCurrentMonthExpenditure() {
		final MonthPage month = getCurrentMonthPage();
		return month.expenditure();
	}

	public Money getBalance() {
		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = repo.theBook();
		return theBook.currentMonth().balance();
	}

	private MonthPage getCurrentMonthPage() {
		final BookRepository repo = Persistence.getRepositoryFactory().
			getBookRepository();

		final Book theBook = repo.theBook();
		final MonthPage month = theBook.currentMonth();
		return month;
	}

}
