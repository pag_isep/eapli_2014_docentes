/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.BaseController;
import eapli.mymoney.application.RegisterIncomeTypeController;
import eapli.util.Console;

/**
 *
 * @author Nuno
 */
public class RegisterIncomeTypeUI extends BaseUI {

	//TODO há bastante duplicação entre RegisterIncomeTypeUi e RegisterExpenseTypeUI
	// deviamos fazer um refactoring
	private RegisterIncomeTypeController controller = new RegisterIncomeTypeController();
	private String incomeType;

	@Override
	public boolean doShow() {
		incomeType = Console.readLine("Enter income type description » ");
		submit(incomeType);

		return true;
	}

	@Override
	public String headline() {
		return "REGISTER AN INCOME TYPE";
	}

	@Override
	protected BaseController controller() {
		return controller;
	}

	private void submit(String type) {
		controller.registerIncomeType(type);
		System.out.println("\nIncome type " + type + " recorded!");
	}
}
