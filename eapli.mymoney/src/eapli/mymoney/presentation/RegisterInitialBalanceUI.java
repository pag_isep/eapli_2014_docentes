/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.mymoney.presentation;

import eapli.mymoney.application.BaseController;
import eapli.mymoney.application.RegisterInitialBalanceController;
//import eapli.expensemanager.domain.Money;
import eapli.framework.model.Money;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author arocha
 */
public class RegisterInitialBalanceUI extends BaseUI {
	private Calendar startDate;
	private Money howMuch;

	private final RegisterInitialBalanceController controller = new RegisterInitialBalanceController();

	@Override
	protected BaseController controller() {
		return controller;
	}

	@Override
	protected boolean doShow() {

		startDate = Console.readCalendar("Start Date (dd-mm-yyyy)? » ");
		final double amount = Console.readDouble("Initial Balance? » ");
		howMuch = Money.euros(amount);

		controller.registerInitialBalance(startDate, howMuch);

		return true;
	}

	@Override
	public String headline() {
		return "Register Initial Balance";
	}
}
