/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.util.Console;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RegisterPaymentMeanUI {

    public void mainLoop() {
        int option = -1;
        while (option != 0) {
            System.out.println("====================================");
            System.out.println("  myMoney - Register Payment Mean  ");
            System.out.println("====================================");
            System.out.println("1. Bank Check Book");
            System.out.println("2. Credit Card");
            System.out.println("3. Debit Card");
            System.out.println("---------------------");
            System.out.println("0. Exit\n\n");

            option = Console.readInteger("Please choose an option");
            switch (option) {
                case 0:
                    System.out.println("bye bye ...");
                    return;
                case 1:
                    final RegisterPaymentMeanBankCheckBookUI uc02check = new RegisterPaymentMeanBankCheckBookUI();
                    uc02check.show();
                    break;
                case 2:
                    final RegisterPaymentMeanCreditCardUI uc02cc = new RegisterPaymentMeanCreditCardUI();
                    uc02cc.show();
                    break;
                case 3:
                    final RegisterPaymentMeanDebitCardUI uc02dc = new RegisterPaymentMeanDebitCardUI();
                    uc02dc.show();
                    break;

                default:
                    System.out.println("option not recognized.");
                    break;
            }
        }
    }
}

