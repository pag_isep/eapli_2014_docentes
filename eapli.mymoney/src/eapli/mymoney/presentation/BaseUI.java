/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.patterns.helpers.DomainEventBase;
import eapli.mymoney.application.BaseController;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author arocha
 */
//TODO should we refactor this class and move it to the framework project?
public abstract class BaseUI {

	public static final String SEPARATOR = "+---------------------------------------------------------------------------------+";
	public static final String BORDER = "+=================================================================================+";

	/**
	 * derived classes should provide the controller object
	 *
	 * an example of the Factory Method and Template Method patterns
	 *
	 * @return
	 */
	protected abstract BaseController controller();

	/**
	 * derived classes should override this method to perform the actual
	 * rendering of the UI
	 *
	 * follows the Template Method pattern
	 *
	 * @return
	 */
	protected abstract boolean doShow();

	/**
	 * derived classes should override this method to provide the title of the
	 * "window"
	 *
	 * @return
	 */
	public abstract String headline();

	public void mainLoop() {
		boolean wantsToExit;
		do {
			wantsToExit = show();
		} while (!wantsToExit);
	}

	public boolean show() {
		drawFormTitle();
		final boolean wantsToExit = doShow();
		showBalances();

		return wantsToExit;
	}

	public void showBalances() {
		drawFormBorder();
		drawFormTitle("Balances: ");
		NumberFormat formater = NumberFormat.getCurrencyInstance(Locale.FRANCE);
		double monthExpenditure = controller().getCurrentMonthExpenditure().
			amount();
		System.out.
			println("Current Month expenditure: " + formater.
				format(monthExpenditure));

		//double weekExpenditure = controller().getThisWeekExpenditure();
		//System.out.println("Weekly expenditure" + nF.format(weekExpenditure));
		double balance = controller().getBalance().amount();
		System.out.
			println("Current balance: " + formater.
				format(balance));

		drawFormBorder();
	}

	protected void drawFormTitle() {
		System.out.println();
		drawFormTitle(headline());
		System.out.println();
	}

	protected void drawFormBorder() {
		System.out.println(BORDER);
		System.out.println();
	}

	protected void drawFormSeparator() {
		System.out.println(SEPARATOR);
	}

	protected void drawFormTitle(final String title) {
		String titleBorder = BORDER.substring(0, 2) + " " + title + " " + BORDER.
			substring(4 + title.length());
		System.out.println(titleBorder);
	}

	/**
	 * AlertLimitObserver is only responsible for showing AlertLimits Messages.
	 */
	protected class AlertLimitObserver implements Observer {

		@Override
		public void update(Observable o, Object arg) {
			DomainEventBase domainEventBase = (DomainEventBase) arg;
			displayAlert(domainEventBase);
		}

		public void displayAlert(DomainEventBase eventBase) {
			System.out.println(eventBase.toString());

		}
	}
}
