/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.RegisterInitialBalanceController;
import eapli.util.Console;

/**
 * The application's main menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu {

	public void mainLoop() {
		if (canDisplayOptions()) {
			showMainMenu();
		}
	}

	private boolean showMainMenu() {
		int option = -1;
		while (option != 0) {
			System.out.println("=============================");
			System.out.println("  myMoney - EXPENSE MANAGER  ");
			System.out.println("=============================\n");
			System.out.println("1. Register an expense");
			System.out.println("2. List expenses of one month");
			System.out.println("300. DEBUG List all expenses");
			System.out.println("--- settings ---");
			System.out.println("20. Set Balance Limit");
			System.out.println("--- master tables ---");
			System.out.println("100. Register an expense type");
			System.out.println("101. List expense types");
			System.out.println("110. Register a Payment Mean");
			System.out.println("---------------------");
			System.out.println("0. Exit\n\n");
			option = Console.readInteger("Please choose an option");
			switch (option) {
				case 0:
					System.out.println("bye bye ...");
					return true;
				case 1:
					final RegisterExpenseUI uc03 = new RegisterExpenseUI();
					uc03.show();
					break;
				case 2:
					final ListExpensesOfMonthUI uc06 = new ListExpensesOfMonthUI();
					uc06.show();
					break;
				case 300:
					final ListExpensesUI uc06debug = new ListExpensesUI();
					uc06debug.show();
					break;
				case 20:
					final RegisterBalanceLimitUI uc20a = new RegisterBalanceLimitUI();
					uc20a.show();
					break;
				case 100:
					final RegisterExpenseTypeUI uc01 = new RegisterExpenseTypeUI();
					uc01.show();
					break;
				case 101:
					final ListExpenseTypesUI uc01_L = new ListExpenseTypesUI();
					uc01_L.show();
					break;
				case 110:
					final RegisterPaymentMeanUI uc02 = new RegisterPaymentMeanUI();
					uc02.mainLoop();
					break;
				default:
					System.out.println("option not recognized.");
					break;
			}
		}
		return false;
	}

	// TODO estes dois métodos deveriam estar noutra classe para que esta respeitasse o Single Responsibility Principle
	private boolean canDisplayOptions() {
		final RegisterInitialBalanceController theInitialBalanceController = new RegisterInitialBalanceController();
		if (theInitialBalanceController.existsInitialBalance() == false) {
			return askForInitialBalance(theInitialBalanceController);
		}
		return true;
	}

	// TODO estes dois métodos deveriam estar noutra classe para que esta respeitasse o Single Responsibility Principle
	private boolean askForInitialBalance(
		RegisterInitialBalanceController theInitialBalanceController) {
		int option = -1;
		while (option != 0) {
			System.out.
				println("=========================================================");
			System.out.
				println("Before using MyMoney you need to input the Initial Balance");
			System.out.println("1. Register Initial Balance");
			System.out.println("0. Exit\n\n");

			option = Console.readInteger("Please choose an option");
			switch (option) {
				case 0:
					System.out.println("bye bye ...");
					return false;
				case 1:
					final RegisterInitialBalanceUI theInitialBalanceUI = new RegisterInitialBalanceUI();
					theInitialBalanceUI.show();
					if (theInitialBalanceController.existsInitialBalance()) {
						return true;
					}
					break;

				default:
					System.out.println("option not recognized.");
					break;
			}
		}
		return true;
	}
}
