/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.model.Money;
import eapli.mymoney.application.BaseController;
import eapli.mymoney.application.ListExpenseTypesController;
import eapli.mymoney.application.RegisterExpenseController;
import eapli.mymoney.domain.BankCheckBook;
import eapli.mymoney.domain.ExpenseType;
import eapli.mymoney.domain.PaymentMean;
import eapli.util.Console;
import eapli.util.Validations;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
class RegisterExpenseUI extends BaseUI {

	private String what;
	private Calendar when;
	private Money howMuch;
	//Another option is only the ExpenseType ID instead object ExpenseType
	private ExpenseType type;
	private PaymentMean paymentMean;
	private String chequeNumber;

	private final RegisterExpenseController controller = new RegisterExpenseController();

	@Override
	protected BaseController controller() {
		return controller;
	}

	@Override
	protected boolean doShow() {
		controller.addObserver(new AlertLimitObserver());

		// Accept valid entries only
		what = readValidEntry("What have you bought? » ");

		when = Console.readCalendar("When did you bought it (dd-mm-yyyy)? » ");

		howMuch = Money.euros(readValidAmount("How much did it cost? » "));

		type = chooseExpenseType();
		if (type == null) {
			return true;
		}

		paymentMean = choosePaymentMean();
		if (paymentMean == null) {
			return true;
		}
		if (paymentMean instanceof BankCheckBook) {
			chequeNumber = readValidEntry("Cheque number:");
		}

		controller.
			registerExpense(what, when, howMuch, type, paymentMean, chequeNumber);

		System.out.println("Expense registered");

		return true;
	}

	@Override
	public String headline() {
		return "Register Expense";
	}

	private ExpenseType chooseExpenseType() {
		List<ExpenseType> types;
		types = new ListExpenseTypesController().getAllExpenseTypes();

		final int option = chooseET(types);
		if (option == 0) {
			return null;
		}
		return types.get(option - 1);
	}

	private int chooseET(List<ExpenseType> list) {
		System.out.println(
			"Choose the expense type in list:\nOption  EXPENSE TYPE");
		int cont = 0;
		for (ExpenseType eType : list) {
			cont++;
			System.out.println("  " + cont + ".  " + eType.description());
		}
		System.out.println("  0.  Exit");
		return Console.readOption(1, list.size(), 0);
	}

	private String readValidEntry(String str) {
		String entry;
		boolean notValid;
		do {
			entry = Console.readLine(str);
			notValid = Validations.isNullOrWhiteSpace(entry);
			if (notValid) {
				System.out.println("Can not be null or white space. Repeat");
			}
		} while (notValid);
		return entry;
	}

	private double readValidAmount(String str) {
		double value;
		do {
			value = Console.readDouble(str);
			if (value <= 0) {
				System.out.println("The cost must be greater than 0.Repeat");
			}
		} while (value <= 0);
		return value;
	}

	private PaymentMean choosePaymentMean() {
		//TODO this code will provably be duplicated with ListExpenseTypesUI
		System.out.println("-- PAYMENT MEANS --");
		List<PaymentMean> paymentMeans = controller.getPaymentMeans();
		int position = 1;
		for (PaymentMean mean : paymentMeans) {
			System.out.println(position + ". " + mean.description());
			position++;
		}
		System.out.println("0. Exit");
		final int option = Console.readOption(1, paymentMeans.size(), 0);
		if (option == 0) {
			return null;
		}
		return paymentMeans.get(option - 1);
	}
}
