/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.BaseController;
import eapli.mymoney.application.RegisterPaymentMeanController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RegisterPaymentMeanCreditCardUI extends BaseUI {

	private String bankName;
	private Calendar validity;
	private long accountNumber;
	private String issuerName;

	RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

	@Override
	protected BaseController controller() {
		return controller;
	}

	@Override
	protected boolean doShow() {
		//FIXME accept valid entries only
		bankName = Console.readLine("Bank Name? » ");
		validity = Console.readCalendar("Validity? » ");
		accountNumber = Console.readInteger("Account Number? » ");
		issuerName = Console.readLine("Issuer Name? » ");

		controller.registerCreditCard(bankName, validity, accountNumber, issuerName);

		return true;
	}

	@Override
	public String headline() {
		return "Register Payment Mean Credit Card";
	}
}
