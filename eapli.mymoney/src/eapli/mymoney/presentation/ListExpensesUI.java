/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.mymoney.application.BaseController;
import eapli.mymoney.application.ListExpensesController;
import eapli.mymoney.domain.Expense;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ListExpensesUI extends BaseUI {

	private final ListExpensesController theController = new ListExpensesController();

	@Override
	protected BaseController controller() {
		return theController;
	}

	@Override
	protected boolean doShow() {
		final List<Expense> expenses = theController.getAllExpenses();

		for (Expense expense : expenses) {
			// FIXME não usar toString() para imprimir objetos
			System.out.println(expense);
		}
		return true;
	}

	@Override
	public String headline() {
		return "List ALL Expenses (DEBUG ONLY)";
	}
}
