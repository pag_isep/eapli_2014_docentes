/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Nuno
 */
public class IncomeTypeRepositoryTest {

	public IncomeTypeRepositoryTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	private transient IncomeTypeRepository instance;

	@Before
	public void setUp() {
		//FIXME there is the need to set-up a clean empty repository
		// for inmemory this could be simple
		// for DB repo it should use dbunit or alike
		instance = Persistence.getRepositoryFactory().getIncomeTypeRepository();
	}

	@After
	public void tearDown() {
	}

	@Test
	public void ensureAddOne() {
		System.out.println("Add one");
		IncomeType incomeType = new IncomeType("abcd");
		instance.add(incomeType);

		long expected = 1;
		long actual = instance.size();
		assertEquals("One added should have count = 1", expected, actual);
	}

	@Test
	public void ensureEmptyHasSizeZero() {
		System.out.println("Empty repository must have count 0");

		long expected = 0;
		long actual = instance.size();
		assertEquals("Empty repository must have count 0", expected, actual);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notAllowedToAddNull() {
		System.out.println("Not Allowed to add null");
		IncomeType incomeType = null;
		instance.add(incomeType);
	}

	@Test(expected = IllegalStateException.class)
	public void notAllowedToAddSameDescription() {
		System.out.println("Not Allowed to add same description");
		IncomeType incomeType = new IncomeType("Mesada");
		instance.add(incomeType);
		incomeType = new IncomeType("Mesada");
		instance.add(incomeType);
	}

	/**
	 * descriptions should not be equal irrespectively of case (upper/lower)
	 */
	@Test(expected = IllegalStateException.class)
	public void notAllowedToAddSameDescriptionCase() {
		System.out.
			println("Not Allowed to add same description irrespectively of case");
		IncomeType incomeType = new IncomeType("Mesada");
		instance.add(incomeType);
		incomeType = new IncomeType("mesada");
		instance.add(incomeType);
	}
}
