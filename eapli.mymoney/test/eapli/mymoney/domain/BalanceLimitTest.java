/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.mymoney.domain.limits.BalanceLimit;
import eapli.framework.model.Money;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author nuno
 */
public class BalanceLimitTest {

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullContructor() {
		new BalanceLimit(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRedLimitNullContructor() {
		Money yelowLimit = Money.euros(0);
		new BalanceLimit(yelowLimit, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testYellowLimitNullContructor() {
		Money redLimit = Money.euros(0);
		new BalanceLimit(null, redLimit);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEqualLimitContructor() {
		Money redLimit = Money.euros(0);
		Money yellowLimit = Money.euros(0);
		new BalanceLimit(yellowLimit, redLimit);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRedLimitGreaterContructor() {
		Money redLimit = Money.euros(10);
		Money yellowLimit = Money.euros(0);
		new BalanceLimit(yellowLimit, redLimit);
	}

	@Test()
	public void testYellowLimit() {
		Money redLimit = Money.euros(0);
		Money yellowLimit = Money.euros(10);
		BalanceLimit balanceLimit = new BalanceLimit(yellowLimit, redLimit);
		assertEquals(yellowLimit, balanceLimit.getYellowLimit());
	}

	@Test()
	public void testRedLimit() {
		Money redLimit = Money.euros(0);
		Money yellowLimit = Money.euros(10);
		BalanceLimit balanceLimit = new BalanceLimit(yellowLimit, redLimit);
		assertEquals(redLimit, balanceLimit.getRedLimit());
	}
}
