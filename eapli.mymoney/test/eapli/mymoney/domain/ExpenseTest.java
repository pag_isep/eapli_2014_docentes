/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.mymoney.domain.Expense;
import eapli.mymoney.domain.Cash;
import eapli.mymoney.domain.ExpenseType;
import eapli.framework.model.Money;
import eapli.mymoney.domain.PaymentMean;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTest {

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDescriptionMustNotBeEmpty() {
		System.out.println("must have non-empty description");
		String what = " ";
		Money howMuch = Money.euros(1);
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDescriptionMustNotBeNull() {
		System.out.println("must have description");
		String what = null;
		Money howMuch = Money.euros(1);
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAmountMustBeBiggerThanZero() {
		System.out.println("must have positive amount");
		String what = "abcd";
		Money howMuch = Money.euros(0);
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAmountMustBePositive() {
		System.out.println("must have positive amount");
		String what = "abcd";
		//FIXME should this a test to the money class and not this class?
		Money howMuch = Money.euros(-1);
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAmountMustNotBeNull() {
		System.out.println("must have description");
		String what = "abcd";
		Money howMuch = null;
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDateOccurredMustNotBeNull() {
		System.out.println("must have description");
		String what = "abcd";
		Money howMuch = Money.euros(1);
		Calendar when = null;
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTypeMustNotBeNull() {
		System.out.println("must have type");
		String what = "abcd";
		Money howMuch = Money.euros(1);
		Calendar when = null;
		ExpenseType type = null;
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
	}

	/**
	 * Test of amount method, of class Expense.
	 */
	@Test
	public void testAmount() {
		System.out.println("amount");
		String what = "abcd";
		Money howMuch = Money.euros(1);
		Calendar when = DateTime.now();
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		Expense instance = new Expense(what, howMuch, when, type, paymentMean, "");
		int result = howMuch.compareTo(instance.amount());
		int expResult = 0;
		assertEquals(expResult, result);
	}

}
