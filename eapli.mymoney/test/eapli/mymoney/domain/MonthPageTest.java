/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.model.Money;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthPageTest {

	private static final int TEST_YEAR = 2014;
	private static final int TEST_MONTH = 03;
	private static final int OTHER_TEST_MONTH = 02;

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}
	private MonthPage instance;

	@Before
	public void setUp() {
		instance = new MonthPage(TEST_YEAR, TEST_MONTH, Money.euros(0));
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of registerExpense method, of class MonthPage.
	 */
	@Test
	public void testCanRegisterExpensesOfMonth() {
		System.out.println("registerExpense");
		String what = "abcd";
		Money howMuch = Money.euros(1);
		Calendar when = DateTime.newCalendar(TEST_YEAR, TEST_MONTH, 1);
		ExpenseType type = new ExpenseType("abcd");
		Payment payment = new Payment(new Cash());
		PaymentMean paymentMean = new Cash();
		instance.registerExpense(what, howMuch, when, type, paymentMean, "");
	}

	/**
	 * Test of registerExpense method, of class MonthPage.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testCanNotRegisterExpensesOfOtherMonth() {
		System.out.println("registerExpense");
		String what = "abcd";
		Money howMuch = Money.euros(1);
		Calendar when = DateTime.newCalendar(TEST_YEAR, OTHER_TEST_MONTH, 1);
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		instance.registerExpense(what, howMuch, when, type, paymentMean, "");
	}

	/**
	 * Test of expenditure method, of class MonthPage.
	 */
	@Test
	public void testGetMonthExpenditure() {
		System.out.println("register two expenses in this month");
		System.out.println("get Month Expenditure");
		String what1 = "abcd";
		Money howMuch1 = Money.euros(10);
		Calendar when1 = DateTime.newCalendar(TEST_YEAR, TEST_MONTH, 1);
		String what2 = "efg";
		Money howMuch2 = Money.euros(20);
		Calendar when2 = DateTime.newCalendar(TEST_YEAR, TEST_MONTH, 2);
		ExpenseType type = new ExpenseType("abcd");
		PaymentMean paymentMean = new Cash();
		instance.registerExpense(what1, howMuch1, when1, type, paymentMean, "");
		instance.registerExpense(what2, howMuch2, when2, type, paymentMean, "");
		Money expResult = Money.euros(30);
		Money result = instance.expenditure();
		assertEquals(expResult.compareTo(result), 0);
	}
}
