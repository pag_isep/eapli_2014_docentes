/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.mymoney.domain;

import eapli.mymoney.domain.IncomeType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Nuno
 */
public class IncomeTypeTest {
    
    public IncomeTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//	@Test
//	public void ensureDescription() {
//		ExpenseType instance = new ExpenseType("some text");
//	}
	@Test(expected = IllegalArgumentException.class)
	public void nullDescriptionNotAllowed() {
		IncomeType instance = new IncomeType(null);
	}
}
